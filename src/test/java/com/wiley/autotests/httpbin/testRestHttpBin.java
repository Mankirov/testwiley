package com.wiley.autotests.httpbin;

import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

public class testRestHttpBin {
        final String URL = "https://httpbin.org/";
        final String URI1 = "delay/9";
        final String URI2 = "image/png";
        int statusCodeOK = 200;
        Long time = 9000L;

        @Test(description = "GET")
        public void getRequestExampleTest1() {
                        given().baseUri(URL)
                                .log().everything()
                                .contentType(ContentType.JSON)
                        .when()
                                .get(URI1)
                        .then()
                                .statusCode(statusCodeOK)
                                .time(greaterThanOrEqualTo(time));
        }

        @Test(description = "GET")
        public void getRequestExampleTest2() {
                byte [] pngAsByteArray =
                        given ()
                                .get(URL + URI2)
                                .asByteArray();

                given().baseUri(URL)
                        .log().everything()
                .when()
                        .get(URI2)
                .then()
                        .statusCode(statusCodeOK)
                        .headers("content-type", URI2)
                        .headers("content-length", String.valueOf(pngAsByteArray.length));
        }
}


























