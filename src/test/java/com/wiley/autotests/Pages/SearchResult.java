package com.wiley.autotests.Pages;

import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchResult extends AbstractPage {
    String javaResultXpath = "//div[@class='products-list']//span[text()='Java']";
    String linkToBookXpath = "//div[@class='products-list']/section/div/a";
    String sectionEachTitleXpath = "//section[@class='product-item ']";
    String buttonAddToCartXpath = "button[contains(text(), 'Add to cart')]";
    int expectedQuantity = 10;
    List<String> javaElements = Arrays.asList("Java", "Java", "Java", "Java", "Java",
                                                    "Java", "Java", "Java", "Java", "Java");

    public SearchResult() {
        super("SearchResult");
    }

    public List<WebElement> webElementList = new ArrayList<>();

    public void checkTheItemsInResultLists() {
        driver.checkTheItemsList(javaResultXpath, javaElements);
    }

    public void checkThatOnlyJavaResultIsDisplayed() {
        driver.checkQuantityOfItems(javaResultXpath, expectedQuantity);
    }

    public void checkEachTitleHasAtLeastOneAddToCartButton() {
        driver.findElementsInNodes(sectionEachTitleXpath, buttonAddToCartXpath);
    }

    public List<WebElement> writeWebElementsList() {
        List<WebElement> webElements = driver.findElements(linkToBookXpath);
        return webElements;
    }

    public void makeSureThereAreSame10TitlesShown(List<WebElement> webElement) {
        driver.equalsListWebElements(webElementList, webElement);
    }
}
