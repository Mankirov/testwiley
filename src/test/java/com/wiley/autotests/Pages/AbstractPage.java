package com.wiley.autotests.Pages;

import com.wiley.autotests.Utils;
import com.wiley.autotests.WDriver;
import org.apache.logging.log4j.Logger;

public abstract class AbstractPage {
    public WDriver driver;
    Logger log;

    String url;
    String pageName;
    String yesButtonXPath = "//button[text()='YES']";

    public AbstractPage(String pageName) {
        this.pageName = pageName;
        this.driver = WDriver.getInstance();
        this.log = Utils.getLoggerInstance();
    }

    public void open() {
        driver.get(url);
        driver.findElement(yesButtonXPath);
        driver.click(yesButtonXPath);
    }

    public void close() {
        driver.close();
    }

    public void quit() {
        driver.quit();
    }
}
