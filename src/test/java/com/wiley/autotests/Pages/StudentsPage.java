package com.wiley.autotests.Pages;

public class StudentsPage extends AbstractPage {
    public StudentsPage() {
        super("Students");
    }

    String urlInStudents = "https://www.wiley.com/en-us/students";

    String studentsHeaderXPath = "//*[text()='Students']";
    String learnMoreXPath = "//*[contains(text(), 'Learn More')]/../..";

    public void checkUrlInStudentsIsOpened() {
        driver.checkUrlIsOpened(urlInStudents);
    }

    public void checkThatStudentsHeaderIsDisplayed() {
        driver.checkTheElementAreDisplayed(studentsHeaderXPath);
    }

    public void checkTheLinksToLearnMore() {
        driver.checkLearnMoreLinksAreDirectToWileyPlus(learnMoreXPath);
    }
}
