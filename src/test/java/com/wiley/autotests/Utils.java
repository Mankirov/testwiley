package com.wiley.autotests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Utils {
    private static Logger log;

    private Utils() {
    }

    public static Logger getLoggerInstance(){
        if (log == null) {
            log = LogManager.getLogger();
        }
        return log;
    }
}

