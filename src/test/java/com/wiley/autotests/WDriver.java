package com.wiley.autotests;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WDriver {
    private static ChromeDriver chromeDriver;
    private static WDriver driver;
    private static Logger log;
    public WebDriverWait wait;
    Actions action;

    private WDriver() {
        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
        chromeDriver = new ChromeDriver();
        chromeDriver.manage().window().maximize();
        log = Utils.getLoggerInstance();
        wait = new WebDriverWait(chromeDriver, 10, 250);
        action = new Actions(chromeDriver);
    }

    public static WDriver getInstance() {
        if (driver == null) {
            driver = new WDriver();
        }
        return driver;
    }

    public void get(String url) {
        log.debug(String.format("Open the page '%s'.", url));
        chromeDriver.get(url);
    }

    public WebElement findElement(String xpath) {
        log.debug(String.format("Start searching for an item, xpath:'%s'", xpath));
        WebElement e;
        try {
            //check is there an element in the html
            e = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            //check if the item is visible on the screen
            e = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
            //check if item is clickable
            e = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        } catch (Exception exception) {
            // if one of the conditions was not met, a screenshot will be taken.
            File screenshot = ((TakesScreenshot)chromeDriver).getScreenshotAs(OutputType.FILE);
            try {
                // save the screenshot taken to the project directory in the reports folder
                FileUtils.copyToDirectory(
                        screenshot,
                        new File("reports/"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            log.error(String.format("Could not find item with locator '%s'.", xpath));
            throw new RuntimeException();
        }
        log.debug(String.format("Element by xpath to locator '%s' found.", xpath));
        return e;
    }

    public List<WebElement> findElements(String xpath) {
        log.debug(String.format("Start searching for items, xpath:'%s'", xpath));
        driver.findElement(xpath);
        log.debug(String.format("Elements by xpath to locator '%s' found.", xpath));
        return chromeDriver.findElements(By.xpath(xpath));
    }

    public void findElementsInNodes(String xpathNodes, String xpathElements) {
        log.debug(String.format("begin find Elements in Nodes: xpath: '&s'" + xpathNodes));
        List<WebElement> nodes = driver.findElements(xpathNodes);
        log.debug(String.format("topElements.size '%s'", nodes.size()));
        for (int i = 1; i <= 10; i++) {
            List<WebElement> elements = nodes.get(i).findElements(By.xpath("descendant::" + xpathElements));
            log.debug(String.format("Elements.size %s", elements.size()));
            if(elements.size() > 0) {
                log.error("Not every title has at least one button “Add to cart”");
                break;
            }
            if(i == 10){
                log.debug("Each header has at least one button “Add to cart”.");
            }
        }
        log.debug(String.format("End of find Elements in Nodes: xpath: '&s'" + xpathNodes));
    }

    public void click(String xpath) {
        log.debug("Click item.");
        WebElement e = driver.findElement(xpath);
        e.click();
        log.debug("Clicked item.");
    }

    public void checkTheElementAreDisplayed(String xpath) {
        log.debug(String.format("Check the link by XPath '%s' are displayed", xpath));
        List<WebElement> webElementList = chromeDriver.findElements(By.xpath(xpath));
        if(webElementList.size() > 0) {
            log.debug(String.format("Link by xpath '%s' are displayed", xpath));
        } else {
            log.error(String.format("Link by xpath '%s' not are displayed", xpath));
        }
    }

    public void moveToElement(String xpath) {
        log.debug(String.format("move to Element by xpath: '%s'", xpath));
        WebElement webElementByXpath = driver.findElement(xpath);
        action.moveToElement(webElementByXpath).build().perform();
        log.debug(String.format("moved to Element by xpath: '%s'", xpath));
    }

    public void checkQuantityOfItems(String xpath, int expectedQuantity) {
        log.debug("Start checking the number of items");
        List<WebElement> webElementList = driver.findElements(xpath);
        log.debug(String.format("Number of items '%s'", webElementList.size()));
        if (webElementList.size() == expectedQuantity) {
            log.debug(String.format("OK!: There are '%s' items", expectedQuantity));
        } else{
            log.error(String.format("Attention!: Is not %s items", expectedQuantity));
            log.error(String.format("Actual number of items: '%s'", webElementList.size()));
        }
        log.debug("Check complete!");
    }

    public void checkTheItemsList(String xpath, List<String> expectedItems) {
        log.debug("Start checking the number of items");
        List<WebElement> webElements = driver.findElements(xpath);
        List<String> actualItems = new ArrayList<>();
        for(WebElement webElement: webElements) {
            actualItems.add(webElement.getAttribute("innerHTML"));
        }
        equalsListString(actualItems, expectedItems);
        log.debug("Check the items list complete!");
    }

    public void equalsListString(List<String> actualItems, List<String> expectedItems) {
        log.debug(String.format("Start to compare lists"));
        Collections.sort(actualItems);
        Collections.sort(expectedItems);
        if(actualItems.containsAll(expectedItems)) {
            log.debug("OK! All items are displayed");
        } else {
            log.error("Attention!: Actual titles do not match the expected");
        }
        log.debug(String.format("End of compare"));
    }

    public void equalsListWebElements(List<WebElement> actualWebElements, List<WebElement> expectedWebElements) {
        log.debug(String.format("Start to compare lists"));
        actualWebElements.containsAll(expectedWebElements);
        log.debug(String.format("End of compare lists"));
    }

    public void checkLearnMoreLinksAreDirectToWileyPlus(String xpath) {
        log.debug("Start checking the links to wileyplus.com");
        List<WebElement> learnMoreList = driver.findElements(xpath);
        if(learnMoreList.size() == 0) {
            log.error("No links to site: WileyPlus.com");
            return;
        }
        for (WebElement learnMoreElement:learnMoreList) {
            if (!(learnMoreElement.getAttribute("href").contains("www.wileyplus.com"))) {
               log.error("No links to site: WileyPlus.com");
               break;
            }
        }
        log.debug("Check the links to wileyplus.com complete!");
    }

    public void checkUrlIsOpened(String url) {
        log.debug(String.format("Check that '%s' url is opened", url));
        if (url.equals(chromeDriver.getCurrentUrl())) {
            log.debug("OK! Correct url" + chromeDriver.getCurrentUrl());
        } else {
            log.error("False! " + chromeDriver.getCurrentUrl());
        }
        log.debug("Check url complete!");
    }

    public void sendKeys(String xpath, String textValue) {
        log.debug(String.format("Enter value '%s'.", textValue));
        WebElement e = driver.findElement(xpath);
        e.sendKeys(textValue);
        log.debug(String.format("Entered value '%s'.", textValue));
    }

    public void clearSearchInput(String xpath) {
        log.debug("Select and delete pre-written text");
        WebElement toClear = driver.findElement(xpath);
        toClear.sendKeys(Keys.CONTROL + "a");
        toClear.sendKeys(Keys.DELETE);
        log.debug("text deleted!");
    }

    public void close() {
        log.debug("Close the current tab.");
        chromeDriver.close();
        log.debug("Closed the current tab.");
    }

    public void quit() {
        log.debug("Close the App. Kill the Task");
        chromeDriver.quit();
        log.debug("Closed the App. Killed the Task");
    }

}
