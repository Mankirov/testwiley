package com.wiley.autotests.PageFragments;

import com.wiley.autotests.Pages.AbstractPage;

import java.util.Arrays;
import java.util.List;

public class PageFragment extends AbstractPage {
    public PageFragment() {
        super("HeaderPage");
    }

    int expectedQuantityOfItemsInWhoWeServe = 11;
    List<String> expectedItems = Arrays.asList("Students", "Instructors", "Book Authors", "Professionals",
            "Researchers", "Institutions", "Librarians", "Corporations", "Societies", "Journal Editors", "Government");

    String whoWeServeXPath = "//a[contains(text(), 'WHO WE SERVE')]";
    String subjectsXPath = "//*[contains(text(), 'SUBJECTS')]";
    String aboutXpath = "//*[contains(text(), 'ABOUT')]";
    String linkToStudentsXpath = "//*[contains(text(), 'Students')]";
    String itemsInWhoWeServeXpath = "//*[@id='Level1NavNode1']/ul/li/a";
    String linkToEducation = "//*[contains(text(), 'Education')]";
    String linkInLogo = "//*[@id=\"main-header-container\"]/div/div[1]/div/div/div/a/img";
    String searchButton = "//button[text()='Search']";
    String searchInput = "//input[@type='search']";
    String searchSuggestionsJava = "//*[text()='Suggestions']/..//span[contains(text(), 'java')]";
    String searchProductsJava = "//*[contains(text(), 'Products')]/..//span[contains(text(), 'Java')]";

    int wordJavaInSuggestions = 4;
    int wordJavaInProduct = 5;

    public void checkTheLinkInTopMenu() {
        driver.checkTheElementAreDisplayed(whoWeServeXPath);
        driver.checkTheElementAreDisplayed(subjectsXPath);
        driver.checkTheElementAreDisplayed(aboutXpath);
    }

    public void checkQuantityOfItemsInWhoWeServe() {
        driver.moveToElement(whoWeServeXPath);
        driver.checkQuantityOfItems(itemsInWhoWeServeXpath, expectedQuantityOfItemsInWhoWeServe);
    }

    public void checkTheItemsInWhoWeServe() {
        driver.checkTheItemsList(whoWeServeXPath, expectedItems);
    }

    public void clickLinkToStudents() {
        driver.moveToElement(whoWeServeXPath);
        driver.click(linkToStudentsXpath);
    }

    public void clickLinkToEducation() {
        driver.moveToElement(subjectsXPath);
        driver.click(linkToEducation);
    }

    public void clickLinkInLogо() {
        driver.click(linkInLogo);
    }

    public void clickSearchButton() {
        driver.click(searchButton);
    }

    public void enterJava() {
        driver.sendKeys(searchInput,"Java");
    }

    public void checkQuantityOfItemsJavaInSuggestion() {
        driver.checkQuantityOfItems(searchSuggestionsJava, wordJavaInSuggestions);
    }

    public void checkQuantityOfItemsJavaInProducts() {
        driver.checkQuantityOfItems(searchProductsJava, wordJavaInProduct);
    }

    public void clearSearchInput() {
        driver.clearSearchInput(searchInput);
    }
}
