package com.wiley.autotests.Pages;

public class HomePage extends AbstractPage {
    public HomePage() {
        super("Home Page");
        this.url = "https://www.wiley.com/en-us";
    }

    public void checkUrlInHomePageIsOpened() {
        driver.checkUrlIsOpened(url);
    }
}
