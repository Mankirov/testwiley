package com.wiley.autotests.Pages;

import java.util.Arrays;
import java.util.List;

public class EducationPage extends AbstractPage {

    public EducationPage() {
        super("Education");
    }

    String educationHeaderXPath = "//*[text()='Education']";
    String subjectsListXPath = "//div[@class='side-panel']/ul/li/a";
    int expectedQuantity = 13;
    List<String> itemsUnderSubjects = Arrays.asList("Information & Library Science", "Education & Public Policy",
            "K-12 General", "Higher Education General", "Vocational Technology",
                "Conflict Resolution & Mediation (School settings)", "Curriculum Tools- General",
                    "Special Educational Needs", "Theory of Education", "Education Special Topics",
                        "Educational Research & Statistics", "Literacy & Reading", "Classroom Management");

    public void checkThatEducationHeaderIsDisplayed() {
        driver.checkTheElementAreDisplayed(educationHeaderXPath);
    }

    public void checkQuantityOfItemsInSubjectsList() {
        driver.checkQuantityOfItems(subjectsListXPath, expectedQuantity);
    }

    public void checkTheItemsInSubjectLists() {
        driver.checkTheItemsList(subjectsListXPath, itemsUnderSubjects);
    }



}
