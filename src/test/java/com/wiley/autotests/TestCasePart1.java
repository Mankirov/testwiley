package com.wiley.autotests;

import com.wiley.autotests.PageFragments.PageFragment;
import com.wiley.autotests.Pages.EducationPage;
import com.wiley.autotests.Pages.HomePage;
import com.wiley.autotests.Pages.SearchResult;
import com.wiley.autotests.Pages.StudentsPage;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class TestCasePart1 {
    PageFragment pageFragment = new PageFragment();
    HomePage homePage = new HomePage();
    StudentsPage studentsPage = new StudentsPage();
    EducationPage educationPage = new EducationPage();
    SearchResult searchResult = new SearchResult();

    @BeforeClass
    public void setUp() {
        homePage.open();
    }

    @Test
    public void testCase1() {
        pageFragment.checkTheLinkInTopMenu();
    }

    @Test
    public void testCase2() {
        pageFragment.checkQuantityOfItemsInWhoWeServe();
        pageFragment.checkTheItemsInWhoWeServe();
    }

    @Test
    public void testCase3() {
        pageFragment.clickLinkToStudents();
        studentsPage.checkUrlInStudentsIsOpened();
        studentsPage.checkThatStudentsHeaderIsDisplayed();
        studentsPage.checkTheLinksToLearnMore();
    }

    @Test
    public void testCase4() {
        pageFragment.clickLinkToEducation();
        educationPage.checkThatEducationHeaderIsDisplayed();
        educationPage.checkQuantityOfItemsInSubjectsList();
        educationPage.checkTheItemsInSubjectLists();
    }

    @Test
    public void testCase5() {
        pageFragment.clickLinkInLogо();
        homePage.checkUrlInHomePageIsOpened();
    }

    @Test
    public void testCase6() {
        homePage.checkUrlInHomePageIsOpened();
        pageFragment.clickSearchButton();
        homePage.checkUrlInHomePageIsOpened();
    }

    @Test
    public void testCase7() {
        pageFragment.enterJava();
        pageFragment.checkQuantityOfItemsJavaInSuggestion();
        pageFragment.checkQuantityOfItemsJavaInProducts();
    }

    @Test
    public void testCase8() {
        pageFragment.clickSearchButton();
        searchResult.webElementList = searchResult.writeWebElementsList();
        searchResult.checkThatOnlyJavaResultIsDisplayed();
        searchResult.checkTheItemsInResultLists();
        searchResult.checkEachTitleHasAtLeastOneAddToCartButton();
    }

    @Test
    public void testCase9() {
        pageFragment.clearSearchInput();
        pageFragment.enterJava();
        pageFragment.clickSearchButton();
        searchResult.makeSureThereAreSame10TitlesShown(searchResult.writeWebElementsList());
    }

    @AfterClass
    public void tearDown() {
//        homePage.quit();
    }
}
